import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationStudentsRulesWrapperComponent } from './graduation-students-rules-wrapper.component';

describe('GraduationStudentsRulesWrapperComponent', () => {
  let component: GraduationStudentsRulesWrapperComponent;
  let fixture: ComponentFixture<GraduationStudentsRulesWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationStudentsRulesWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationStudentsRulesWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
