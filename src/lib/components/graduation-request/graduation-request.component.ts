import { Component, OnInit, OnDestroy, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { LoadingService } from '@universis/common';
import { GraduationRequestService } from './../../services/graduation-request/graduation-request.service';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestWizardDirective } from './../../directives/graduation-request-step-directive';

@Component({
  selector: 'universis-graduation-request',
  templateUrl: './graduation-request.component.html'
})
export class GraduationRequestComponent implements OnInit, OnDestroy {
  // This component is based on this example
  // https://angular.io/guide/dynamic-component-loader

  @ViewChild(GraduationRequestWizardDirective) GraduationRequestWizard: GraduationRequestWizardDirective;

  /**
   * The observable that holds the graduation status
   */
  public graduationStatusAsObservable$: BehaviorSubject<any>;

  /**
   * The form submission for the graduation form
   */
  private currentStepOutput: Subscription;

  /**
   * The list of the wizard steps
   */
  public wizardSteps: any;

  /**
   * The current active step index
   */
  public currentAdIndex = -1;

  /**
   * The current active step object
   */
  public activeStep;

  /**
   * Indicates whether the graduation request wizard should be shown
   */
  public canShowRequest = false;


  constructor(
    private _graduationRequestService: GraduationRequestService,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _loadingService: LoadingService,
    public viewContainerRef: ViewContainerRef
  ) { }

  public graduationRequestStatus: GraduationRequestStatus;

  async ngOnInit() {
    try {
      this.graduationStatusAsObservable$ = new BehaviorSubject(undefined);
      await this.updateRequestStatusData();
      this.loadComponent(0);
    } catch (err) {
      console.error(err);
    }
  }

  ngOnDestroy() {
    if (this.currentStepOutput) {
      this.currentStepOutput.unsubscribe();
    }
  }
  
  /**
   * 
   * Updates the graduation request status
   * 
   * @param newStatus 
   * 
   */
  private async updateRequestStatusData(graduationRequestStatus?: any): Promise<void> {
    try {
      if (graduationRequestStatus) {
        this.graduationRequestStatus = graduationRequestStatus;
      } else {
        this.graduationRequestStatus = await this._graduationRequestService.getGraduationRequestStatus();
      }

      this.graduationStatusAsObservable$.next(this.graduationRequestStatus);
      this.canShowRequest = this.shouldShowWizard(this.graduationRequestStatus);
      console.log(this.graduationRequestStatus);
    } catch (err) {
      console.error(err);
    }
  }

  /**
   *
   * Shows the component (step) at the wizard
   *
   * @param index The index inside the steps array to show
   *
   */
  loadComponent(index: number): void {
    if (!this.graduationRequestStatus.graduationRequestPeriodOpen || this.graduationRequestStatus.steps[index].status === 'unavailable') {
      return;
    }

    if (this.currentStepOutput) {
      this.currentStepOutput.unsubscribe();
    }

    const component = this.graduationRequestStatus.steps[index].component.component;
    this.activeStep = this.graduationRequestStatus.steps[index];
    this.currentAdIndex = index;
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
    const viewContainerRef = this.GraduationRequestWizard.viewContainerRef;
    viewContainerRef.clear();
    const currentComponent = viewContainerRef.createComponent(componentFactory);
    currentComponent.instance.graduationRequestStatusObservable$ = this.graduationStatusAsObservable$;

    // special configuration for components
    if (this.activeStep.alternateName === 'graduationRequest') {
      this.currentStepOutput = currentComponent.instance.formSubmit.subscribe((value) => this.submitGraduationForm(value));
    }

    if (this.activeStep.alternateName === 'graduationDocumentsSubmission') {
      this.currentStepOutput = currentComponent.instance.outputAction.subscribe(
        async (event) => this.handleDocumentsSubmissionStepOutput(event)
      );
    }
  }

  /**
   *
   * Submits the graduation form
   *
   * @param value The graduation request form submission
   *
   */
  async submitGraduationForm(data) {
    try {
      this._loadingService.showLoading();
      const newStatus = await this._graduationRequestService.setGraduationRequest(data.graduationEventId, data.form);
      await this.updateRequestStatusData(newStatus);
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Handles the document submission events
   *
   * @param event The data passed by the documents submission step
   *
   */
  async handleDocumentsSubmissionStepOutput(event) {
    this._loadingService.showLoading();
    try {
      let newStatus;
      if (event.action === 'upload') {
        newStatus = await this._graduationRequestService.uploadGraduationRequestAttachment(event.data);
      } else if (event.action === 'remove') {
        newStatus = await this._graduationRequestService.removeGraduationRequestAttachment(event.data);
      } else if (event.action === 'download') {
        newStatus = await this._graduationRequestService.downloadGraduationRequestAttachment(event.data);
      }
      await this.updateRequestStatusData(newStatus);
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Decides whether the graduation wizard should be shown
   *
   * @param graduationRequestStatus  The graduation request status object
   *
   */
  shouldShowWizard(graduationRequestStatus) {
    return (
      graduationRequestStatus
      && graduationRequestStatus.graduationPeriodExist
      && graduationRequestStatus.graduationRequestPeriodOpen
    );
  }

  /**
   *
   * triggers a file download
   *
   * @param blob The file as received from the server
   */
  triggerFileDownload(blob: Blob, fileName: string) {
    const objectUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = objectUrl;
    const extension = 'txt'; // TODO: change the extension
    a.download = `${fileName}-.${extension}`;
    a.click();
    window.URL.revokeObjectURL(objectUrl);
    a.remove();
  }
}
