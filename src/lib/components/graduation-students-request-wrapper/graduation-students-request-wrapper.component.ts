import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@universis/common';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestService } from './../../services/graduation-request/graduation-request.service';

@Component({
  selector: 'lib-graduation-students-request-wrapper',
  templateUrl: './graduation-students-request-wrapper.component.html'
})
export class GraduationStudentsRequestWrapperComponent implements OnInit {

  public graduationRequestStatus: GraduationRequestStatus;

  constructor(
    private _graduationRequestService: GraduationRequestService,
    private _loadingService: LoadingService
  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.graduationRequestStatus = await this._graduationRequestService.getGraduationRequestStatus();
    } catch (err) {
      console.error('graduation: ', err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

}
