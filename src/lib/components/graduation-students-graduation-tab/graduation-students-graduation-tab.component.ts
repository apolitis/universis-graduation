import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@universis/common';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestService } from './../../services/graduation-request/graduation-request.service';

@Component({
  selector: 'universis-degree-graduation',
  templateUrl: './graduation-students-graduation-tab.component.html'
})
export class GraduationStudentsGraduationTabComponent implements OnInit {

  /**
   *
   * The title of the message box. Contains basic information about the semester
   *
   */
  public graduationPeriodStatusTitle: string;

  /**
   *
   * The message the user will see regarding the graduation period
   *
   */
  public graduationPeriodStatusMessage: string;


  /**
   *
   * A flag that indicates wether the current date is a graduation period for the user's department
   *
   */
  public inGraduationPeriod: boolean;

  /**
   *
   * Information regarding the graduation request
   *
   */
  public graduationRequestStatus: GraduationRequestStatus;


  constructor(
    private _router: Router,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _graduationRequestService: GraduationRequestService
  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.graduationRequestStatus = await this._graduationRequestService.getGraduationRequestStatus();
      this.graduationPeriodStatusTitle = this.getTitle(this.graduationRequestStatus);
      this.graduationPeriodStatusMessage = this.getMessage(this.graduationRequestStatus);
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Forms the title of the message box
   *
   * @param graduationRequestStatus The status of the graduation request
   *
   */
  private getTitle(graduationRequestStatus: GraduationRequestStatus): string {
    const academicPeriod = this._translateService.instant(
      `UniversisGraduationModule.semester.${this.graduationRequestStatus.academicPeriod}`
    );
    const semester = this._translateService.instant(`UniversisGraduationModule.semester.caps.title`);

    return `${academicPeriod} ${semester} ${graduationRequestStatus.academicYear}`;
  }

  /**
   *
   * Calculates the message of the message box
   *
   * @param graduationRequestStatus The status of the graduation request
   *
   */
  private getMessage(graduationRequestStatus: GraduationRequestStatus): string {
    if (!graduationRequestStatus.graduationRequestPeriodOpen) {
      return this._translateService.instant('UniversisGraduationModule.OutOfGraduationRequestPeriodMessage');
    } else if (
      graduationRequestStatus.graduationRequestPeriodOpen
      && graduationRequestStatus.status
    ) {
      return this._translateService.instant('UniversisGraduationModule.InGraduationRequestPeriodMessage', {
        semesterPeriod: this._translateService.instant(`UniversisGraduationModule.semester.${graduationRequestStatus.academicPeriod}`),
        academicYear: graduationRequestStatus.academicYear
      });
    } else if (
      graduationRequestStatus.graduationRequestPeriodOpen
      && graduationRequestStatus.status === 'PotentialActionStatus'
    ) {
      return this._translateService.instant('UniversisGraduationModule.GraduationRequestSubmittedMessage');
    }

    return '';
  }

  /**
   *
   * Change the location to the graduation request
   *
   */
  public navigateToGraduationRequest() {
    this._router.navigate(['/degree/graduation-request']);
  }
}
