import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationStudentsLayoutComponent } from './graduation-students-layout.component';

describe('GraduationStudentsLayoutComponent', () => {
  let component: GraduationStudentsLayoutComponent;
  let fixture: ComponentFixture<GraduationStudentsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationStudentsLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationStudentsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
