import { TestBed } from '@angular/core/testing';

import { GraduationRequestStudentService } from './graduation-request-student.service';

describe('GraduationRequestStudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraduationRequestStudentService = TestBed.get(GraduationRequestStudentService);
    expect(service).toBeTruthy();
  });
});
