import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { GraduationStudentsLayoutComponent } from './components/graduation-students-layout/graduation-students-layout.component';
import { GraduationStudentsHomeComponent } from './components/graduation-students-home/graduation-students-home.component';
import { GraduationRulesComponent } from './components/graduation-rules/graduation-rules.component';
import { GraduationProgressComponent } from './components/graduation-progress/graduation-progress.component';
import {
  GraduationStudentsGraduationTabComponent
} from './components/graduation-students-graduation-tab/graduation-students-graduation-tab.component';
import {
  GraduationStudentsRequestWrapperComponent
} from './components/graduation-students-request-wrapper/graduation-students-request-wrapper.component';
import { 
  GraduationStudentsRulesWrapperComponent
} from './components/graduation-students-rules-wrapper/graduation-students-rules-wrapper.component';

export const studentRoutes: Routes = [
  {
    path: '',
    component: GraduationStudentsLayoutComponent,
    canActivate: [
        AuthGuard
    ],
    children: [
      {
        path: 'overview',
        component: GraduationStudentsHomeComponent,
        children: [
          {
            path: 'prerequisites',
            redirectTo: 'prerequisites/graduation-rules'
          },
          {
            path: 'prerequisites',
            component: GraduationStudentsRulesWrapperComponent,
            children: [
              {
                path: 'graduation-rules',
                component: GraduationRulesComponent
              },
              {
                path: 'graduation-progress',
                component: GraduationProgressComponent
              }
            ]
          },
          {
            path: 'graduation',
            component: GraduationStudentsGraduationTabComponent
          }
        ]
      },
      {
        path: 'graduation-request',
        component: GraduationStudentsRequestWrapperComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(studentRoutes)],
  exports: [RouterModule]
})
export class GraduationStudentsRoutingModule { }
