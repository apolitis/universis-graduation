import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraduationRequestComponent } from './components/graduation-request/graduation-request.component';
import { GraduationStudentsHomeComponent } from './components/graduation-students-home/graduation-students-home.component';
import { GraduationRulesComponent } from './components/graduation-rules/graduation-rules.component';
import { GraduationProgressComponent } from './components/graduation-progress/graduation-progress.component';
import {
  GraduationStudentsGraduationTabComponent
} from './components/graduation-students-graduation-tab/graduation-students-graduation-tab.component';
import {
  GraduationStudentsRulesWrapperComponent
} from './components/graduation-students-rules-wrapper/graduation-students-rules-wrapper.component';


export const graduationRequestRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'graduation-request',
        component: GraduationRequestComponent
      }, {
        path: 'overview',
        component: GraduationStudentsHomeComponent,
        children: [
          {
            path: 'prerequisites',
            redirectTo: 'prerequisites/graduation-rules'
          },
          {
            path: 'prerequisites',
            component: GraduationStudentsRulesWrapperComponent,
            children: [
              {
                path: 'graduation-rules',
                component: GraduationRulesComponent
              },
              {
                path: 'graduation-progress',
                component: GraduationProgressComponent
              }
            ]
          },
          {
            path: 'graduation',
            component: GraduationStudentsGraduationTabComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(graduationRequestRoutes)],
  exports: [RouterModule]
})
export class GraduationRoutingModule {
  constructor() {}
}
